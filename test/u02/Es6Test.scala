package u02

import org.junit.jupiter.api.Assertions._
import org.junit.jupiter.api.Test
import u02.es6.Es6

class Es6Test {

  val es6 = Es6

  @Test def testFibonacci {
    assertEquals(es6.fibonacci(0), 0)
    assertEquals(es6.fibonacci(1), 1)
    assertEquals(es6.fibonacci(5), 5)
    assertEquals(es6.fibonacci(10), 55)
  }

  @Test def testFibonacciWithTailRecursion {
    assertEquals(es6.fibonacciTail(0), 0)
    assertEquals(es6.fibonacciTail(1), 1)
    assertEquals(es6.fibonacciTail(5), 5)
    assertEquals(es6.fibonacciTail(10), 55)
  }

}
