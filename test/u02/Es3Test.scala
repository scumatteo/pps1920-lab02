package u02

import org.junit.jupiter.api.Assertions._
import org.junit.jupiter.api.Test
import u02.es3.Es3

class Es3Test {

    val parityCalculator = Es3.ParityCalculator
    val negativeCalculator = Es3.NegCalculator
    val negativeCalculatorWithGenerics = Es3.NegCalculatorWithGenerics
    val empty: String => Boolean = _==""
    val notEmpty = negativeCalculator.neg(empty)
    val notEmptyMethod = negativeCalculator.negMethod(empty)

    val emptyInt: Int => Boolean = _ % 2 == 0
    val notEmptyMethodWithGenerics = negativeCalculatorWithGenerics.negMethod(emptyInt)


    @Test def testParity(){
      assertEquals(parityCalculator.parity(2), "Even")
      assertEquals(parityCalculator.parity(1), "Odd")
    }

    @Test def testParityMethod(){
      assertEquals(parityCalculator.parityMethod(2), "Even")
      assertEquals(parityCalculator.parityMethod(1), "Odd")
    }

    @Test def testNeg(){
      assertFalse(notEmpty(""))
      assertTrue(notEmpty("foo"))
      assertTrue(notEmpty("foo") && !notEmpty(""))
    }

    @Test def testNegMethod() {
      assertFalse(notEmptyMethod(""))
      assertTrue(notEmptyMethod("foo"))
      assertTrue(notEmptyMethod("foo") && !notEmptyMethod(""))
    }

  @Test def testNegMethodWithGenerics() {
    assertFalse(notEmptyMethodWithGenerics(2))
    assertTrue(notEmptyMethodWithGenerics(1))
  }
}
