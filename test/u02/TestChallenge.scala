package u02

import org.junit.jupiter.api.Assertions._
import org.junit.jupiter.api.Test
import u02.challenge.Challenge

import Challenge.Option._

class TestChallenge {

  val optionals = Challenge.Option

  @Test def testChallenge(){
    assertEquals(optionals.challengeFunction(Some(true))(!_), Some(false))
    assertEquals(optionals.challengeFunction(Some(2))(_*2), Some(4))
  }

}
