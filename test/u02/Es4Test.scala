package u02

import org.junit.jupiter.api.Assertions._
import org.junit.jupiter.api.Test
import u02.es4.Es4

class Es4Test {
  import u02.es4.Es4

  val es4 = Es4

  @Test def TestCurriedFunType(){
    assertTrue(es4.p1(3)(4)(5))
    assertFalse(es4.p1(3)(4)(3))
  }

  @Test def TestNotCurriedFunType(){
    assertTrue(es4.p2(3, 4, 5))
    assertFalse(es4.p2(3, 4, 3))
  }

  @Test def TestCurriedMethodType(){
    assertTrue(es4.p3(3)(4)(5))
    assertFalse(es4.p3(3)(4)(3))
  }

  @Test def TestNotCurriedMethodType(){
    assertTrue(es4.p4(3, 4, 5))
    assertFalse(es4.p4(3, 4, 3))
  }
}
