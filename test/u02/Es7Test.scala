package u02

import org.junit.jupiter.api.Assertions._
import org.junit.jupiter.api.Test
import scala.math._
import u02.es7.Es7

class Es7Test {

  val es7 = Es7
  val calculator = Es7.calculator

  val squareSide = 8.0
  val square = es7.Square(squareSide)

  val rectBase = 5.0
  val rectHeight = 7.0
  val rectangle = es7.Rectangle(rectBase, rectHeight)

  val circleRound = 5.0
  val circle = es7.Circle(circleRound)

  @Test def testCirclePerimeter(){
    assertEquals(calculator.perimeter(circle), circleRound * 2 * Pi)
  }

  @Test def testRectanglePerimeter(){
    assertEquals(calculator.perimeter(rectangle), 2 * (rectBase+ rectHeight))
  }

  @Test def testSquarePerimeter(){
    assertEquals(calculator.perimeter(square), squareSide * 4)
  }

  @Test def testCircleArea(){
    assertEquals(calculator.area(circle), pow(circleRound, 2) * Pi)
  }

  @Test def testRectangleArea(){
    assertEquals(calculator.area(rectangle), rectBase * rectHeight)
  }

  @Test def testSquareArea(){
    assertEquals(calculator.area(square), pow(squareSide, 2))
  }



}
