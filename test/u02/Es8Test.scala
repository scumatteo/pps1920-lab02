package u02

import org.junit.jupiter.api.Assertions._
import org.junit.jupiter.api.Test

import es8.Es8.Option._

class Es8Test {

  @Test def testFilter(){
    assertEquals(filter(Some(5))(_ > 2), Some(5))
    assertEquals(filter(Some(5))(_ < 2), None())
    assertEquals(filter(Some(5))(_ >= 5), Some(5))
    assertEquals(filter(Some(5))(_ <= 5), Some(5))
    assertEquals(filter(None[Int])(_ < 2), None())
  }

  @Test def testMap(){
    assertEquals(map(Some(5))(_ > 2), Some(true))
    assertEquals(map(Some(5))(_*2), Some(10))
    assertEquals(map(None[Int])(_ > 2), None())
  }

  @Test def testMap2(){
    assertEquals(map2(Some(5), Some(5))(_ * _), Some(25))
    assertEquals(map2(Some(2), Some(5))(_ + _), Some(7))
    assertEquals(map2(Some("ciao"), Some(" come stai?"))(_ + _), Some("ciao come stai?"))
    assertEquals(map2(Some(""), None())(_ + _), None())
  }

}
