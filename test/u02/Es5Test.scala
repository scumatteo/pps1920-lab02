package u02

import org.junit.jupiter.api.Assertions._
import org.junit.jupiter.api.Test
import u02.es5.Es5

class Es5Test {

  val es5 = Es5

  @Test def testCompose(){
    assertEquals(es5.compose(_-1, _*2)(5), 9)
  }

  @Test def testComposeWithGenerics(){
    assertEquals(es5.composeWithGenerics[Int](_+1, _*2)(5), 11)
    assertEquals(es5.composeWithGenerics[String](_+"Come stai?", _+ " ")("Ciao!"), "Ciao! Come stai?")
  }

}
