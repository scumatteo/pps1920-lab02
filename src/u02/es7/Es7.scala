package u02.es7

import scala.math._

object Es7 extends App{
  sealed trait Shape
  case class Rectangle(base: Double, height: Double) extends Shape
  case class Square(side: Double) extends Shape
  case class Circle(round: Double) extends Shape

  object calculator{
    def perimeter(shape: Shape): Double = shape match{
      case Circle(r) => r * 2 * Pi
      case Rectangle(b, h) => 2 * (b+h)
      case Square(l) =>  l * 4
    }

    def area(shape: Shape): Double = shape match {
      case Circle(r) => pow(r, 2) * Pi
      case Rectangle(b, h) => b * h
      case Square(l) => pow(l, 2)
    }
  }


}
