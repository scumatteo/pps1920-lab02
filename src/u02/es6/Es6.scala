package u02.es6

object Es6 extends App{

  def fibonacci(n: Int): Int = n match {
    case 0 => 0
    case 1 => 1
    case _ => fibonacci(n - 1) + fibonacci(n - 2)
  }

  def fibonacciTail(n: Int): Int = {
    def _fib(n: Int, prev: Int, current: Int): Int = n match {
      case 0 => current
      case _ => _fib(n - 1, prev + current, prev)
    }
    _fib(n, 1, 0)
  }

}
