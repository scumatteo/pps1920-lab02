package u02.es3

object Es3 extends App{

  object ParityCalculator{
    val parity: Int => String = {
      case v if (v % 2 == 0) => "Even"
      case _ => "Odd"
    }

    def parityMethod(v: Int): String = v match {
      case v if v % 2 == 0 => "Even"
      case _ => "Odd"
    }
  }

  object NegCalculator{
    val neg: (String => Boolean) => (String => Boolean) = f => s => !f(s)
    def negMethod(f : String =>  Boolean) : (String => Boolean) = p => !f(p)
  }

  object NegCalculatorWithGenerics{
    def negMethod[A](f : A =>  Boolean) : (A => Boolean) = p => !f(p)
  }

}
