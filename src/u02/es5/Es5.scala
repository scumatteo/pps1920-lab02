package u02.es5

object Es5 extends App{

  def compose(f: Int => Int, g: Int => Int) : Int => Int = p => f(g(p))

  def composeWithGenerics[A](f: A => A, g: A => A): A => A = p => f(g(p))

}
