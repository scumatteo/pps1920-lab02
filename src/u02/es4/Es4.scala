package u02.es4

object Es4 extends App {
  val p1: Int => Int => Int => Boolean = x => y => z => x <= y && y <= z
  val p2: (Int, Int, Int) => Boolean = (x, y, z) => x <= y && y <= z
  def p3 = p1
  def p4 = p2
}
